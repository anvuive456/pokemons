import { Component, OnInit } from '@angular/core';
import {Pokemon} from "../../model/pokemon";
import {PokemonServiceService} from "../../service/pokemon-service.service";

@Component({
  selector: 'app-pokemonlist',
  templateUrl: './pokemonlist.component.html',
  styleUrls: ['./pokemonlist.component.css']
})
export class PokemonlistComponent implements OnInit {
  pokemons: Pokemon[] =[];
  constructor(private  service:PokemonServiceService) { }

  ngOnInit(): void {
    this.service.getPkms(0).subscribe(pkms => {
      console.log(pkms);
      this.pokemons = pkms.results;
    });
  }


}
