import {Component, Input, OnInit} from '@angular/core';
import {PokemonDetail} from 'src/app/model/pokemondetail';
import {PokemonServiceService} from "../../service/pokemon-service.service";
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-pokemondetail',
  templateUrl: './pokemondetail.component.html',
  styleUrls: ['./pokemondetail.component.css']
})
export class PokemondetailComponent implements OnInit {
  url: string = '';

  pkm: PokemonDetail = {
    name: '',
  };

  constructor(private  service: PokemonServiceService,private route:ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
        this.url = String(params.get('url'));


        return this.url;
      })
    );
    this.service.getDetailPkm(this.url).subscribe(pkm => this.pkm = pkm);
  }

}
