import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from "../../model/pokemon";
import {Router,ActivatedRoute} from "@angular/router";
import {PokemonServiceService} from "../../service/pokemon-service.service";

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css']
})
export class PokemonItemComponent implements OnInit {
  @Input() pkm:Pokemon = {
    name: '',
    url: ''
  };

  constructor(private  activatedRoute:ActivatedRoute,
              private router:Router,
              private service:PokemonServiceService) {
  }

  ngOnInit(): void {
  }

  moveToDetail(url:string){
    this.router.navigate(['/pokemondetail', { url: url }]);
  }

}
