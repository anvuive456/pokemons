import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Pokemon} from "../model/pokemon";
import {Pokemonlist} from "../model/pokemonlist";


@Injectable({
  providedIn: 'root'
})
export class PokemonServiceService {
  pokemonUrl = 'https://pokeapi.co/api/v2/pokemon';

  pokemonLimit(offset?: number): string {
    return this.pokemonUrl+`?offset=${offset}&limit=20`;
  };

  constructor(private  http: HttpClient) {
  }

  getPkms(offset:number = 0): Observable<Pokemonlist> {
    console.log(this.http.get<Pokemonlist>(this.pokemonLimit(offset)));
    return this.http.get<Pokemonlist>(this.pokemonLimit(offset));
  }

  getDetailPkm(url:string):Observable<Pokemon>{
    console.log(this.http.get<Pokemon>(url));
    return  this.http.get<Pokemon>(url);
  }


}
