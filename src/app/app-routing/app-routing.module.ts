import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PokemonlistComponent} from "../components/pokemonlist/pokemonlist.component";
import {PokemondetailComponent} from "../components/pokemondetail/pokemondetail.component";

const routes:Routes =[
  { path:'',redirectTo:'pokemons',pathMatch:'full'},
  { path:'pokemons',component:PokemonlistComponent},
  { path:'pokemondetail/:url',component:PokemondetailComponent,},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
