import {Pokemon} from "./pokemon";

export class Pokemonlist {
  next?: string;
  previous?: string;
  count: number = 0;
  results: Pokemon[] = [];
}
